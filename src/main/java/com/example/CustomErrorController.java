package com.example;

import org.springframework.boot.autoconfigure.web.ErrorController;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.util.Arrays;
import java.util.Collections;

@RestController
public class CustomErrorController implements ErrorController {
  @RequestMapping(value = "/error")
  public ResponseEntity<String> error(HttpServletRequest request, HttpServletResponse response) {
    System.out.println("IN error controller");
    return new ResponseEntity("Error raised in custom error controller", HttpStatus.UNAUTHORIZED);
  }

  @Override
  public String getErrorPath() {
    System.out.println("Get error path called");
    return null;
  }
}
