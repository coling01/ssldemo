package com.example;

import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.security.Principal;
import java.time.LocalDateTime;


@RestController
public class DemoController {
  @PreAuthorize("hasAuthority('ROLE_USER')")
  @RequestMapping(value="/demo")
  public String demo(Model model, Principal principal){
    String principalName="null";
    if(principal!=null){
      principalName=principal.getName();
    }
    LocalDateTime now= LocalDateTime.now();
    return "demo authenticated with name:"+principalName+" at:"+now.toLocalTime();
  }
}
