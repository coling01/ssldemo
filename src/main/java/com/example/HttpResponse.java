package com.example;

import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;
import java.util.Objects;

class HttpResponse {
  private int statusCode;
  private List<String> errors;

  HttpResponse(int statusCode, List<String> errors) {
    this.statusCode = statusCode;
    this.errors = errors;
  }

  @JsonProperty("statusCode")
  public int getStatusCode() {
    return this.statusCode;
  }

  @JsonProperty("errors")
  public List<String> getErrors() {
    return this.errors;
  }

}
